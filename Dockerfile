# For more information, please refer to https://aka.ms/vscode-docker-python
ARG ODOO_VERSION=15.0
FROM odoo:${ODOO_VERSION}
# USER root

LABEL MAINTAINER Dixon Martinez <https://gitlab.com/damartinez>

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-python-configure-containers
# RUN adduser -u 5678 --disabled-password --gecos "" odoo && chown -R appuser /app
# RUN adduser -u 5678 --disabled-password --gecos "" odoo && chown -R appuser .
USER odoo

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install pip requirements
COPY requirements.txt .
RUN pip3 install -r requirements.txt
