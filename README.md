# odoo-docker-compose

## Getting started

## Preparación Inicial

1. [Instalar docker](https://docs.docker.com/get-docker/)
2. [Instalar docker-compose](<https://docs.docker.com/compose/install/>)

## Instalación de Odoo con docker

1. [Descargar el repositorio](https://gitlab.com/damartinez/odoo-docker-compose)

~~~
git clone https://gitlab.com/damartinez/odoo-docker-compose.git
~~~

2. Ingresar a la carpeta

~~~
cd odoo-docker-compose
~~~

3. Crear archivo .env, odoo.conf y docker-compose.yaml

~~~
cp copy.env .env
cp copy.docker-compose.yml docker-compose.yml
cp config/copy.odoo.conf config/odoo.conf

Si se estará usando VSCode como editor se debe realizar lo siguiente
cp .vscode/launch.json.copy .vscode/launch.json
code .
~~~

4. Editar parámetros de .env

~~~
Ejemplo:
Este parámetro es para indicar la versión de odoo que deseas instalar
ODOO_VERSION=16
Indica el puerto web para acceder a odoo
WEB_PORT=8069
Indica el puerto para acceder depurar los módulos que estén en la carpeta addons
DEBUG_PORT=8888

Indica la versión de postgres que deseas utilizar 
DB_TAG=13
Indica el puerto de comunicación de base de datos
DB_PORT=5437
Usuario y contraseña para el usuario odoo que se conectará a la base de datos
DB_USER=odoo_db
DB_PASSWD=odoo_db

OPTIONS=--dev=all
~~~

5. Editar parámetros de conf/odoo.conf

~~~
Ejemplo:
Contraseña administrador para crear, respaldar y restaurar base de datos desde odoo
admin_passwd = 123456
Estos valores deben corresponder con los valores del archivo .env
~~~

6. Editar parámetros de .vscode/launc.json

~~~
Ejemplo:
"localRoot": "PATH_ROUTE/odoo-docker-compose/addons",
Estos valores deben corresponder con los valores del archivo .env
"port": 8069,
"debugServer": 8888,
~~~

7. Opcional: Editar docker-compose.yml, esto siempre y cuando se requiera añadir nuevos servicios o modificar parámetros.
8. Ejecutar docker compose

~~~
docker compose up -d
~~~
